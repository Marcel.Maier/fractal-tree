var angle = 0
var sliderAngle;
var sliderChildBranchLength;
var sliderBranches;
var randomLengthCheckBox;
var regenerate;
var radio;

function setup() {

    createCanvas(500, 500);

    sliderAngle = createSlider(0, TWO_PI, PI/4, 0.01);
    sliderAngle.position(520, 0);
    sliderChildBranchLength = createSlider(0, .75, 0.45, 0.001);
    sliderChildBranchLength.position(520, 20);
    sliderBranches = createSlider(1, 8, 2, 1);
    sliderBranches.position(520, 40);
    randomLengthCheckBox= createCheckbox('Random branch length and angle', false);
    randomLengthCheckBox.position(520, 65);
    regenerate = createButton('Regenerate');
    regenerate.position(520, 90);
    regenerate.attribute("disabled", true);
    radio = createRadio();
    radio.option('Normal Tree Color', 0);
    radio.option('Random Rainbow Color', 1);
    radio.position(520, 120);

    noLoop();

    sliderAngle.input(meinUpdate);
    sliderBranches.input(meinUpdate);
    sliderChildBranchLength.input(meinUpdate);
    randomLengthCheckBox.input(meinUpdate);
    regenerate.mousePressed(redraw);
    radio.input(redraw);
}

function meinUpdate() {
    if(randomLengthCheckBox.checked()) {
        sliderAngle.attribute("disabled", true);
        sliderChildBranchLength.attribute("disabled", true);
        regenerate.removeAttribute('disabled');
    } else {
        sliderAngle.removeAttribute('disabled');
        sliderChildBranchLength.removeAttribute('disabled');
        regenerate.attribute("disabled", true);
    }
    redraw();
}

function draw() {
    angle = sliderAngle.value();
    var color1 = color(0, 0, 153);
    var color2 = color(204, 51, 0);
    setGradient(0, 0, width, height, color1, color2, "Y");
    translate(250, height - 100);
    fill('rgb(72, 99, 35)');
    ellipse(0, 100, 530, 200);
    strokeWeight(6);
    branch(80);
}

function branch(len) {

    var r = radio.value();

    if(r == 1) {
        var RGBColor1 = Math.floor(Math.random() * 256);
        var RGBColor2 = Math.floor(Math.random() * 256);
        var RGBColor3 = Math.floor(Math.random() * 256);
        stroke(RGBColor1, RGBColor2, RGBColor3);        
    } else {
        stroke(139,90,43);
    }

    line(0, 0, 0, -len);
    translate(0, -len);
    strokeWeight(3);
    const branches = sliderBranches.value();
    const branchesPerSide = ~~(branches / 2);
    if(len > 10) {
        for (let index = 0; index < branchesPerSide; index++) {
            push();
            rotate(-angle + (index * (angle / branchesPerSide)));
            branch(getLength(len));
            pop();
            push();
            rotate(angle - (index * (angle / branchesPerSide)));
            branch(getLength(len));
            pop();
        }
        if((branchesPerSide * 2) != branches) {
            branch(getLength(len));
        }
    } else {
        if(r != 1) {
            stroke(54, 123, 0);
            ellipse(0, 0, 3, 3);
        }
    }
}

function getLength(len) {
    const childBranchLength = sliderChildBranchLength.value();
    const random = randomLengthCheckBox.checked();
    if(random == true) {
        return len * Math.random();
    } else {
        return len * childBranchLength;
    }
}

function setGradient(x, y, w, h, c1, c2, axis) {
    noFill();
    if (axis == "Y") {  // Top to bottom gradient
      for (let i = y; i <= y+h; i++) {
        var inter = map(i, y, y+h, 0, 1);
        var c = lerpColor(c1, c2, inter);
        stroke(c);
        line(x, i, x+w, i);
      }
    }  
    else if (axis == "X") {  // Left to right gradient
      for (let j = x; j <= x+w; j++) {
        var inter2 = map(j, x, x+w, 0, 1);
        var d = lerpColor(c1, c2, inter2);
        stroke(d);
        line(j, y, j, y+h);
      }
    }
  }